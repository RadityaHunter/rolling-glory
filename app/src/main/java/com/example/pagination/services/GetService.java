package com.example.pagination.services;

import com.example.pagination.model.Title;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetService {

    // getData
    @GET("/api/v2/gifts")
    Call<Title> getTitle(@Query("page[number]") int pagenumber,
                         @Query("page[size]") int pagesize);
}