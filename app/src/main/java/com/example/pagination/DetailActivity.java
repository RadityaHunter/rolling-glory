package com.example.pagination;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pagination.model.FavoritModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DetailActivity extends AppCompatActivity {

    Realm realm;
    RealmHelper realmHelper;
    FavoritModel favoritModel;

    int id;
    TextView tvId;

    boolean favState = false;
    List<FavoritModel> favoritModelList;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.imgCovers)
    ImageView imgCover;
    @BindView(R.id.tvNames)
    TextView tvName;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.tvReviewDetail)
    TextView tvReviewDetail;
    @BindView(R.id.tvPointDetail)
    TextView tvPointDetail;
    @BindView(R.id.ratingBarDetail)
    RatingBar ratingBarDetail;
    @BindView(R.id.imgFav_)
    ImageView imgFav;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.tvDeskripsi)
    TextView tvDeskripsi;

    @BindView(R.id.imgIsNewDetail)
    ImageView imgIsNewDetail;
    @BindView(R.id.tvInfo)
    TextView tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_);
        ButterKnife.bind(this);

        //Set up Realm
        Realm.init(DetailActivity.this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            id = bundle.getInt("id");
            Picasso.get().load(bundle.getString("img")).into(imgCover);
            tvName.setText(bundle.getString("name"));
            tvPointDetail.setText(String.valueOf(bundle.getInt("point")));
            ratingBarDetail.setRating(bundle.getFloat("rating"));
            tvReviewDetail.setText(String.valueOf(bundle.getInt("review")));
            tvDeskripsi.setText(bundle.getString("deskripsi"));

            if (bundle.getInt("isnew") == 1) {
                imgIsNewDetail.setVisibility(View.VISIBLE);
            } else {
                imgIsNewDetail.setVisibility(View.GONE);
            }

            tvInfo.setText(bundle.getString("info"));

            //tvId.setText(String.valueOf(id));
        }

        loadFav();
        listerner();

    }

    private void loadFav() {

        favoritModelList = new ArrayList<>();

        realmHelper = new RealmHelper(realm);
        favoritModelList = realmHelper.getAllFav();
        int arr[] = new int[favoritModelList.size() + 1];
        for (int i = 0; i < favoritModelList.size(); i++) {
            arr[i] = favoritModelList.get(i).getId_item();
        }

        if (check(arr, id)) {
            imgFav.setImageResource(R.drawable.ic_baseline_favorite_24);
            favState = true;
            //Toast.makeText(getApplicationContext(), "ID find!",Toast.LENGTH_SHORT).show();
        } else {
            imgFav.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            favState = false;
            //Toast.makeText(getApplicationContext(), "No ID find!",Toast.LENGTH_SHORT).show();
        }
    }


    private boolean check(int[] arr, int toCheckValue) {
        // check if the specified element
        // is present in the array or not
        // using Linear Search method
        boolean test = false;
        for (int element : arr) {
            if (element == toCheckValue) {
                test = true;
                break;
            }
        }
        return test;
    }

    private void listerner() {
        imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (favState) {
                    favState = false;
                    imgFav.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    realmHelper = new RealmHelper(realm);
                    realmHelper.delete(id);
                    Toast.makeText(getApplicationContext(), "Remove Fav!",
                            Toast.LENGTH_SHORT).show();

                } else {
                    favState = true;
                    imgFav.setImageResource(R.drawable.ic_baseline_favorite_24);
                    favoritModel = new FavoritModel();
                    favoritModel.setId_item(id);
                    favoritModel.setStatus("mark");

                    realmHelper = new RealmHelper(realm);
                    realmHelper.save(favoritModel);
                    Toast.makeText(getApplicationContext(), "Add Fav!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
                startActivity(intent);
                finish();

            }
        });
    }
}