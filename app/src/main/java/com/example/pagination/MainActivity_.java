package com.example.pagination;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pagination.adapter.RecylerAdapter;
import com.example.pagination.model.Attributes;
import com.example.pagination.model.Datum;
import com.example.pagination.model.Title;
import com.example.pagination.services.ApiClient;
import com.example.pagination.services.GetService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity_ extends AppCompatActivity {

    NestedScrollView nestedScrollView;
    List<Datum> datumList;
    GridLayoutManager layoutManager;
    RecylerAdapter recylerAdapter;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;

    private int page_number = 1;
    private int item_count = 4;


    // var pagination
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previous_total = 0;
    private int view_threshold = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);
        ButterKnife.bind(this);

        init();
        getData();
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    page_number++;
                    performPagination();
                }
            }
        });
    }

    private void getData() {
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity_.this, "Tunggu...", "Please Wait....");
        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<Title> call = service.getTitle(page_number, item_count);
        call.enqueue(new Callback<Title>() {
            @Override
            public void onResponse(Call<Title> call, Response<Title> response) {
                progressDialog.dismiss();

                //Log.d("test", response.body().getData().get(0).getAttributes().getImages().get(0));

                datumList = response.body().getData();
                //Log.d("test", datumList.get(1).getAttributes().getImages().get(0));

                List<Attributes> attributes = new ArrayList<>();
                for (int i = 0; i < datumList.size(); i++) {
                    attributes.add(datumList.get(i).getAttributes());
                }

                //PhotoAdapter adapter = new PhotoAdapter(MainActivity.this, attributes);
                recylerAdapter = new RecylerAdapter(attributes, MainActivity_.this);
                if (rv != null) {
                    rv.setAdapter(recylerAdapter);
                }
            }

            @Override
            public void onFailure(Call<Title> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity_.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /*rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previous_total) {
                            isLoading = false;
                            previous_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleItems + view_threshold)) {
                        page_number++;
                        performPagination();
                        isLoading = true;
                    }
                }
            }
        });*/
    }

    private void performPagination() {
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity_.this, "Tunggu...", "Please Wait....");
        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<Title> call = service.getTitle(page_number, item_count);
        call.enqueue(new Callback<Title>() {
            @Override
            public void onResponse(Call<Title> call, Response<Title> response) {

                datumList = response.body().getData();
                //Log.d("test2", datumList.get(1).getAttributes().getImages().get(0));

                if (datumList != null) {
                    List<Attributes> attributes = new ArrayList<>();
                    for (int i = 0; i < datumList.size(); i++) {
                        attributes.add(datumList.get(i).getAttributes());
                    }
                    recylerAdapter.addAttributes(attributes);
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Title> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity_.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init() {
        rv = findViewById(R.id.rv);
        nestedScrollView = findViewById(R.id.scroll_view);
        layoutManager = new GridLayoutManager(MainActivity_.this, 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);
    }


}