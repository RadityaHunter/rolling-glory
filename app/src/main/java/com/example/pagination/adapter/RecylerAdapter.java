package com.example.pagination.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pagination.DetailActivity;
import com.example.pagination.R;
import com.example.pagination.RealmHelper;
import com.example.pagination.model.Attributes;
import com.example.pagination.model.FavoritModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RecylerAdapter extends RecyclerView.Adapter<RecylerAdapter.MyViewHolder> {

    private List<Attributes> attributesLIst;
    private Context context;

    Realm realm;
    RealmHelper realmHelper;
    FavoritModel favoritModel;

    List<FavoritModel> favoritModelList;

    public RecylerAdapter(List<Attributes> attributesLIst, Context context) {
        this.attributesLIst = attributesLIst;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //Set up Realm
        Realm.init(context);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);

        Attributes attributes = attributesLIst.get(position);

        favoritModelList = new ArrayList<>();

        realmHelper = new RealmHelper(realm);
        favoritModelList = realmHelper.getAllFav();
        int arr[] = new int[favoritModelList.size() + 1];
        for (int i = 0; i < favoritModelList.size(); i++) {
            arr[i] = favoritModelList.get(i).getId_item();
        }

        if (check(arr, attributes.getId())) {
            holder.imgFavs.setImageResource(R.drawable.ic_baseline_favorite_24);
        } else {
            holder.imgFavs.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        }

        holder.tvName.setText(attributes.getName());
        holder.tvReview.setText(String.valueOf(attributes.getNumOfReviews()) + " reviews");
        holder.tvPoint.setText(String.valueOf(attributes.getPoints() ));
        holder.ratingBar.setRating(attributes.getRating());

        if (attributes.getIsNew() == 1) {
            holder.imgIsNew.setVisibility(View.VISIBLE);
        } else {
            holder.imgIsNew.setVisibility(View.GONE);
        }

        Picasso.get().load(attributes.getImages().get(0)).into(holder.imgCover);
        holder.cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("id", attributes.getId());
                intent.putExtra("name", attributes.getName());
                intent.putExtra("img", attributes.getImages().get(0));
                intent.putExtra("point", attributes.getPoints());
                intent.putExtra("rating", attributes.getRating());
                intent.putExtra("review", attributes.getNumOfReviews());
                intent.putExtra("isnew", attributes.getIsNew());
                intent.putExtra("info", attributes.getInfo());
                intent.putExtra("deskripsi", attributes.getDescription());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return attributesLIst.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvPoint, tvReview;
        private ImageView imgCover, imgFavs, imgIsNew;
        private ConstraintLayout cl;
        private RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            imgCover = itemView.findViewById(R.id.img);
            cl = itemView.findViewById(R.id.cls);

            tvPoint = itemView.findViewById(R.id.tvPoint);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            tvReview = itemView.findViewById(R.id.tvReview);
            imgIsNew = itemView.findViewById(R.id.imgIsNew);
            imgFavs = itemView.findViewById(R.id.imgFavs);
        }
    }

    public void addAttributes(List<Attributes> attributes) {
        for (Attributes im : attributes) {
            attributesLIst.add(im);
        }
        notifyDataSetChanged();
    }

    private boolean check(int[] arr, int toCheckValue) {
        // check if the specified element
        // is present in the array or not
        // using Linear Search method
        boolean test = false;
        for (int element : arr) {
            if (element == toCheckValue) {
                test = true;
                break;
            }
        }
        return test;
    }
}
