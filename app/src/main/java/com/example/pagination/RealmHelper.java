package com.example.pagination;

import android.util.Log;

import com.example.pagination.model.FavoritModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {

    Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    // untuk menyimpan data
    public void save(final FavoritModel favoritModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "Database was created");
                    Number currentIdNum = realm.where(FavoritModel.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    favoritModel.setId(nextId);
                    FavoritModel model = realm.copyToRealm(favoritModel);
                } else {
                    Log.e("ppppp", "execute: Database not Exist");
                }
            }
        });
    }

    // untuk memanggil semua data
    public List<FavoritModel> getAllFav() {
        RealmResults<FavoritModel> results = realm.where(FavoritModel.class).findAll();
        return results;
    }

    // untuk menghapus data
    public void delete(Integer id) {
        final RealmResults<FavoritModel> model = realm.where(FavoritModel.class).equalTo("id_item", id).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                model.deleteAllFromRealm();
            }
        });
    }

}
