package com.example.pagination.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FavoritModel extends RealmObject {

    @PrimaryKey
    private Integer id;
    private Integer id_item;
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_item() {
        return id_item;
    }

    public void setId_item(Integer id_item) {
        this.id_item = id_item;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
